package Task2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class LocalDate {

	public static void main(String[] args) {
		newDate (currentDate(), 10, 5, 8);
			
	}
	
	
	
	
	
	
	
	public static Date currentDate() {
		Date currentDate = new Date();
		System.out.println("Current date is: " + currentDate);
		return currentDate;
	}

	
	public static Date newDate(Date currentDate, int addYear, int addMonth, int addDay) {
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, addYear);
		cal.add(Calendar.MONTH, addMonth);
		cal.add(Calendar.DATE, addDay);
		String newDate = (String)(df.format(cal.getTime()));
		System.out.println("New date is: " + newDate);
		return cal.getTime();
				
	}

}
	
