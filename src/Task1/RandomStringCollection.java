package Task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomStringCollection {

	public static void main(String[] args) {
		createList(5);		
	}
	
	
	public static void createList(int countStrings) {
		
		List<String> list = new ArrayList<String>();
		
		for (int i = 0; i < countStrings; i++) {
			list.add(generateRandomString(generateRandomLength()));
		}
		System.out.println(list);
	}
	
	
	public static int generateRandomLength() {
		
		int minNumber = 6;
		int maxNumber = 15;
		int randomLength = ThreadLocalRandom.current().nextInt(minNumber, maxNumber + 1);
		//System.out.println(randomLength);
		return randomLength;
	}
	
	
	public static String generateRandomString(int randomLength) {
		String letters = new String ("abcdefghijklmnopqrstuvwxyz");
		StringBuffer finalString = new StringBuffer();
		
		int randomIndex = 0;
		for (int i=0; i < randomLength; i++) {
			randomIndex = new Random().nextInt(letters.length());
			finalString.append(letters.charAt(randomIndex));
		}
		//System.out.println(finalString);
		return finalString.toString();
	}

}
