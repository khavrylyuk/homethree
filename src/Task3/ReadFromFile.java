package Task3;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class ReadFromFile {

	public static void main(String[] args) throws Exception {
		try {
			writeHashMapToFile("FilteredTeams.txt", readFileToHashMap("SerieA.txt"));
		}
		catch (FileNotFoundException ex) {
			System.out.println("File not found!");
		}
		catch (IOException ex) {
			System.out.println("Something happend!");
		}
		

		
	}
	
	public static HashMap<Integer, String> readFileToHashMap (String file) throws Exception {
		
		FileReader fr = new FileReader(file);
		Scanner scan = new Scanner(fr);
		
		HashMap<Integer, String> teams = new HashMap<Integer, String>();
		
		try {
			int i = 1;
			while (scan.hasNextLine()) {
				teams.put(i, scan.nextLine());
				i++;
			}
		System.out.println("Key : Value");
		System.out.println("-----------");
		teams.forEach((key, value) -> System.out.println(key + "   : " + value));
						
		} finally {
			fr.close();
			scan.close();
		}
		return teams;
	}	
	
	
	public static void writeHashMapToFile (String fileToWrite, HashMap<Integer, String> teams) throws IOException {
		FileWriter fw = new FileWriter(fileToWrite);
		BufferedWriter bw = new BufferedWriter(fw);
		System.out.println ("========================");
		System.out.println("");
		System.out.println ("Values, whose keys are power of 2");
		System.out.println("------------------------");
		try {
			int power = 2;
			for (int i = 1; i < teams.size()+1 ; i++) {
				boolean number = (int)(Math.log(i)/Math.log(power))==(Math.log(i)/Math.log(power));
				if (number == true) {
					bw.write(teams.get(i) + "\n");
					System.out.println(teams.get(i));
				}
			}
			
		} finally {		
			bw.close();
			fw.close();
		}
	}
	
		
}
